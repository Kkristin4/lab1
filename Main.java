package com.company;
import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        BufferedReader br = null;
        try{
            File file = new File("File.txt");
            if(!file.exists())
            file.createNewFile();
            br = new BufferedReader(new FileReader("File.txt"));
            String line;
            String word = "";
            char symbol;
            int count = 0;
            Map map = new HashMap();
            while((line = br.readLine()) != null){
                for (int i = 0; i < line.length(); i++) {
                    symbol = line.charAt(i);
                    if ((((int) symbol >= 65) && ((int) symbol <= 90)) || (((int) symbol >= 97) && ((int) symbol <= 122))) {
                        word = word + symbol;
                    }
                    if ((symbol == ' ') || (i == line.length() - 1)) {
                        if (map.containsKey(word)) {
                            map.replace(word, ((int) map.get(word) + 1));
                            count++;
                        } else {
                            map.put(word, 1);
                            count++;
                        }
                        word = "";
                    }

                }
            }
            ArrayList<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(map.entrySet());
            list.sort(new Comparator<Map.Entry<String, Integer>>(){
                        public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                            return o1.getValue().compareTo(o2.getValue());
                        }
                });
            try(FileWriter writer = new FileWriter("Table.csv", false))
            {
                for (int i = map.size() - 1; i >= 0; i--) {
                    writer.write(list.get(i).getKey() + "," + (double)(list.get(i).getValue()) * 100 / count + "%");
                    writer.append('\n');
                }
            }
            catch(IOException ex){
                System.out.println(ex.getMessage());
            }
        }
        catch (IOException e){
            System.out.print("ERROR" + e);
        }finally {
            try {
                br.close();
            }
            catch (IOException e){
                System.out.println("Error" + e);
            }
        }
    }
}
